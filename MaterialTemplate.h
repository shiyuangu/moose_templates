/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */

#include "Material.h"

#ifndef NAME_TO_BE_REPLACED_H
#define NAME_TO_BE_REPLACED_H

//Forward Declarations
class NAME_TO_BE_REPLACED;

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>();

class NAME_TO_BE_REPLACED : public Material
{
public:
  NAME_TO_BE_REPLACED(const std::string & name,
                  InputParameters parameters);

protected:
  virtual void computeQpProperties();

private:

  //TODO: define material properties like this:
  //MaterialProperty<Real> & _diffusivity;
};

#endif //NAME_TO_BE_REPLACED_H
